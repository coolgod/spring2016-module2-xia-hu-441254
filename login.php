<?php
session_start();

// if already logged in, jump to home page
if  ( isset($_SESSION['username']) && $_SESSION['username'] != '' ) {
	header("Location: home.php");
	exit();
}

// if it's a submit
if ($_SERVER["REQUEST_METHOD"] == "POST") {

	if(isset($_POST['username'])) {
		// username check
		$username = $_POST['username'];
		if( !preg_match('/^[\w_\-]+$/', $username) ){
			echo "Invalid username! page refresh in 5 seconds!";
			header("refresh:5;url=login.php");
			exit;
		}

		$file = fopen("user.txt", "r");
		if ( $file ) {
			while ( ($line = fgets($file)) !== false ) {
				$line = rtrim($line, "\r\n"); //important
				if ( $line === $username ) {
					$_SESSION['username'] = $line;
					header("Location: home.php");
					exit();
				}
			}
			fclose($file);
			echo "username not exists! page refresh in 5 seconds!";
			header("refresh:5;url=login.php");
		}
		exit();
	}	
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>file sharing site login</title>
	<link rel="stylesheet" href="./css/style.css" type="text/css" />
</head>
<body>
	<form method="POST" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>">
		<label for="username">username: </label>
		<input id="username" type="text" name="username" value="" required/><br />
		<input type="submit" name="submit" value="login" />
	</form>
</body>
</html>