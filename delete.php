<!DOCTYPE html>
<html lang="en">
	<head>
		<link rel="stylesheet" href="./css/style.css" type="text/css" />
	</head>
	<body>

<?php 
session_start();

// log in check
if  ( !(isset($_SESSION['username']) && $_SESSION['username'] != '') ) {
	header("Location: login.php");
	exit();
}

// single file delete
if ( isset($_GET['filename']) ) {
	$filename = $_GET['filename'];

	// check filename
	if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
		echo "Invalid filename";
	}else{
		if ( unlink($_SESSION['user_dir'].$filename) ){
			echo "delete succeed! page refresh in 5 seconds!";
		}else{
			echo "delete fail! page refresh in 5 seconds!";
		}
	}	
}

// multiple file select and delete by POST
else if ( isset($_POST['filename']) ) {
	//echo var_dump($_POST['filename']);
	$filenames = $_POST['filename'];
	foreach($filenames as $filename){
		// check filename
		if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
			echo "Invalid filename: ".$filename;
		}else{
			if( unlink($_SESSION['user_dir'].$filename) ){
				echo $filename."......delete succeed!"."<br />";
			}else{
				echo $filename."......delete failed!"."<br />";
			}
		}
	}
	echo "page refresh in 5 seconds!";
}else{
	echo "invalid parameters!";
}

?>

	</body>
</html>
<?php 
	header("refresh:5;url=home.php");
	exit();
?>