<!DOCTYPE html>
<html lang="en">
	<head>
		<link rel="stylesheet" href="./css/style.css" type="text/css" />
	</head>
	<body>

<?php
	session_start();

	// log in check
	if  ( !(isset($_SESSION['username']) && $_SESSION['username'] != '') ) {
		header("Location: login.php");
		exit();
	}

	if ( isset($_FILES['uploadedfile']) ){
		$filename = $_FILES['uploadedfile']['name'];
 
		// filename check
		if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
			echo "Invalid filename! page refresh in 5 seconds!";
			header("refresh:5;url=home.php");
			exit();
		}
		
		$username = $_SESSION['username'];		 
		$full_path = sprintf("./%s/%s", $username, $filename);
		// echo $full_path;

		if( move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $full_path) ){
			echo "upload succeed! page refresh in 5 seconds!";
		}else{
			echo "upload fail! page refresh in 5 seconds!";
		}
	}
?>
	</body>
</html>
<?php 
	header("refresh:5;url=home.php");
	exit();
?>