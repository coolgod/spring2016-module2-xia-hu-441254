<?php
session_start();

// log in check
if  ( !(isset($_SESSION['username']) && $_SESSION['username'] != '') ) {
	header("Location: login.php");
	exit();
}

// read user directory
$_SESSION['user_dir'] =  "./".$_SESSION['username']."/";
$files = scandir($_SESSION['user_dir']);
?>

<!DOCTYPE html>
<html lang="en">
 <head>
 	<meta charset="utf-8" />
 	<title>file sharing site</title>
 	<link rel="stylesheet" href="./css/style.css" type="text/css" />
 	<link rel="stylesheet" href="./css/home.css" type="text/css" />
 </head>
 <body>
 	<!-- header -->
 	<div id="header">
 		<p>hello, <?php echo $_SESSION['username'] ?></p>
 		<a href="logout.php">logout</a>
 	</div>

 	<!-- file list -->
 	<div id="main">
 	<form method="POST" action="delete.php">
	<p>
		<input type="submit" value="delete" id="multi-delete-btn" />
	</p>
 	<ul>
 		<?php foreach ($files as $index=>$filename) { ?>
 			<?php 
 				if ( $index != 0 && $index != 1 ) {
 			?>
 			<li>
 					<input class="multi-delete-box" type="checkbox" name="filename[]" value="<?php echo $filename ?>" />
 					<a href="view.php?filename=<?php echo $filename ?>">
 						<?php 
 							echo htmlentities($filename);
 						?>
 					</a>
 					<a class="delete" href="delete.php?filename=<?php echo htmlentities($filename) ?>">
 						delete
 					</a>
 			</li>
 		<?php 
 				}
 		}
 		?>
 	</ul>
	 </form>

	 	<!-- upload -->
	 	<form enctype="multipart/form-data" action="upload.php" method="POST">
		
			<input type="hidden" name="MAX_FILE_SIZE" value="20000000" />
			<p>Choose a file to upload:</p>
			<input name="uploadedfile" type="file" id="uploadfile_input"/>
		<p>
			<input type="submit" value="upload" />
		</p>
		</form>

	</div>

 </body>
</html>