<?php
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$param1 = $_POST['param1'];
		$param2 = $_POST['param2'];
		$opt = $_POST['operation'];
		$result = 0;
		if ( $opt == "add" ) {
			$result = $param1 + $param2;
		}else if ( $opt == "sub" ) {
			$result = $param1 - $param2;
		}else if ( $opt == "mult" ) {
			$result = $param1 * $param2;
		}else if ( $opt == "div" ) {
			if ( $param2 == 0 ) {
				echo "dividend can't be zero!";
				exit();
			}
			$result = $param1 / $param2;
		}else{
			echo "wrong operation!";
			exit();
		}
		echo "result: ".$result;
		exit();
	}
?>

<!DOCTYPE html>
<html lang="en">
 <head>
 	<meta charset="utf-8" />
 	<title>calculator</title>
 </head>
 <body>
 	<form method="post" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>">
 		<label for="param1">parameter 1</label>
 		<input id="param1" type="text" name="param1" value="" pattern="[0-9]+\.[0-9]+|[0-9]*" title="rational number only"/><br />
 		<br />
 		<label for="param2">parameter 2</label>
 		<input id="param2" type="text" name="param2" value="" pattern="[0-9]+\.[0-9]+|[0-9]*" title="rational number only"/><br />
 		<input id="opt1" type="radio" name="operation" value="add" checked /><label for="opt1">add</label><br />
 		<input id="opt2" type="radio" name="operation" value="sub" /><label for="opt2">substract</label><br />
 		<input id="opt3" type="radio" name="operation" value="mult" /><label for="opt3">multiply</label><br />
 		<input id="opt4" type="radio" name="operation" value="div" /><label for="opt4">divide</label><br />
 		<input type="submit" name="submit" value="calculate" />
 	</form>
 </body>
</html>