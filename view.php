<?php 
session_start();

// log in check
if  ( !(isset($_SESSION['username']) && $_SESSION['username'] != '') ) {
	header("Location: login.php");
	exit();
}

if ( isset($_GET['filename']) ) {
	$filename = $_GET['filename'];
	 
	// check filename
	if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
		echo "Invalid filename. page refresh in 5 seconds!";
		header("refresh:5;url=home.php");
		exit();
	}

	$full_path = sprintf(".\%s\%s", $_SESSION['username'], $filename);
 
	// get the MIME type
	$finfo = new finfo(FILEINFO_MIME_TYPE);
	$mime = $finfo->file($full_path);
	 
	// set the Content-Type header to the MIME type
	header("Content-Type: ".$mime);
	readfile($full_path);
}
?>

